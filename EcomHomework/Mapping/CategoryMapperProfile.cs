﻿using System;
using EcomHomework.Core.Dtos;
using EcomHomework.Core.Mapping;
using EcomHomework.Core.Models;
using EcomHomework.Core.Services.Paging;
using EcomHomework.Models;
using EcomHomework.Services.Categories;

namespace EcomHomework.Mapping
{
    public class CategoryMapperProfile : EcomHomeworkMapperProfile<Category, CategoryDto, CategoryCommandDto>
    {
        public CategoryMapperProfile()
        {
            CreateMap<Category, CategoryDto>()
                .IncludeBase<IBaseEntity, ResponseDto>()
                .ForMember(dto => dto.Id, options => options
                    .MapFrom((entity, dto) =>
                    {
                        dto.Id = entity.Id != Guid.Empty ? entity.Id : (Guid?) null;
                        return dto.Id;
                    }));

            CreateMap<IPagedList<Category>, IPagedList<CategoryDto>>();

            CreateMap<CategoryCommandDto, Category>()
                .IncludeBase<CommandDto, IBaseEntity>();
        }
    }
}