﻿using System;
using EcomHomework.Core.Models;

namespace EcomHomework.Models
{
    public class Category : BaseEntity
    {
        public Guid Id { get; set; }
        public long Cx { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}