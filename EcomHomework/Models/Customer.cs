﻿using System;
using EcomHomework.Core.Models;
using Microsoft.AspNetCore.Identity;

namespace EcomHomework.Models
{
    public class Customer : IdentityUser, IBaseEntity
    {
        public long Cx { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime DeletedAt { get; set; }
    }
}