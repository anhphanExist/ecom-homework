﻿using EcomHomework.Core.Validators;
using EcomHomework.Services.Categories;
using FluentValidation;

namespace EcomHomework.Validators
{
    public class CategoryValidator : EcomHomeworkValidator<CategoryCommandDto>
    {
        public CategoryValidator()
        {
            RuleFor(x => x.Name)
                .NotNull()
                .NotEmpty()
                .MaximumLength(200);
        }
    }
}