﻿using EcomHomework.Core.Validators;
using EcomHomework.Filters;
using Microsoft.Extensions.DependencyInjection;
using EcomHomework.Events;
using EcomHomework.Services;

namespace EcomHomework.Infrastructure
{
    public static class DependencyRegister
    {
        public static void RegisterServicesContainer(this IServiceCollection services)
        {
            //register core module
            services.RegisterCoreContainer();
            
            //register business logic layer (service)
            services.RegisterServicesLayer();
            
            //register event logic
            services.RegisterEvents();
            
            //register filters
            services.RegisterFilters();
        }
        
        private static void RegisterCoreContainer(this IServiceCollection services)
        {
            services.AddScoped<ValidateModelAttribute>();
        }
    }
}