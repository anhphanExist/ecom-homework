﻿using EcomHomework.Core.Middlewares.ExceptionHandlers;
using EcomHomework.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace EcomHomework.Infrastructure.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        /// <summary>
        /// Configure the application HTTP request pipeline
        /// </summary>
        /// <param name="app">Builder for configuring an application's request pipeline</param>
        /// <param name="env"></param>
        /// <param name="logger"></param>
        /// <param name="dbContext"></param>
        public static void ConfigureRequestPipeline(this IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger,
            ApplicationDbContext dbContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("v1/swagger.json", "EcomHomework v1");
                    c.DocExpansion(DocExpansion.None);
                });
            }
            else
            {
                app.UseHsts();
            }

            app.ConfigureExceptionHandler(env, logger);

            app.UseCors();

            app.UseHttpsRedirection();

            app.UseSerilogRequestLogging();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            
            dbContext.Database.Migrate();
        }
    }
}