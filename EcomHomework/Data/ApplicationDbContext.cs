﻿using System;
using System.Threading;
using System.Threading.Tasks;
using EcomHomework.Core.Models;
using EcomHomework.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace EcomHomework.Data
{
    public class ApplicationDbContext : IdentityDbContext<Customer>
    {
        #region Fields

        public DbSet<Category> Categories { get; set; }

        #endregion

        #region Ctor

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            
        }

        #endregion

        #region Utilities

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Category>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .IsClustered(false);
                entity.HasIndex(e => e.Cx)
                    .IsClustered();

                entity.Property(e => e.Id).ValueGeneratedNever();
                entity.Property(e => e.Cx).ValueGeneratedOnAdd().Metadata
                    .SetAfterSaveBehavior(PropertySaveBehavior.Ignore);
            });

            builder.Entity<Customer>(entity =>
            {
                entity.HasKey(e => e.Id).IsClustered(false);
                entity.HasIndex(e => e.Cx).IsClustered();
            });
        }

        protected virtual void UpdateEntityState()
        {
            var now = DateTime.UtcNow;

            foreach (var changedEntity in ChangeTracker.Entries())
            {
                if (changedEntity.Entity is IBaseEntity entity)
                {
                    switch (changedEntity.State)
                    {
                        case EntityState.Added:
                            entity.CreatedAt = now;
                            entity.UpdatedAt = now;
                            break;
                        case EntityState.Modified:
                            Entry(entity).Property(x => x.CreatedAt).IsModified = false;
                            entity.UpdatedAt = now;
                            break;
                        case EntityState.Deleted:
                            entity.IsDeleted = true;
                            entity.DeletedAt = now;
                            changedEntity.State = EntityState.Modified;
                            break;
                    }
                }
            }
        }

        #endregion

        #region Methods

        public override int SaveChanges()
        {
            UpdateEntityState();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            UpdateEntityState();
            return base.SaveChangesAsync(cancellationToken);
        }

        #endregion
        
    }
}