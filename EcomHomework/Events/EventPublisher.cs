﻿using System;
using System.Collections.Generic;
using EcomHomework.Core.Events;
using Microsoft.Extensions.DependencyInjection;

namespace EcomHomework.Events
{
    /// <summary>
    /// Represents an event publisher implementation
    /// </summary>
    public class EventPublisher : IEventPublisher
    {
        private readonly IServiceProvider _serviceProvider;
        
        public EventPublisher(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public virtual void Publish<TEvent>(TEvent @event)
        {
            //get all event consumers
            var consumers = _serviceProvider.GetServices(typeof(IConsumer<TEvent>)) as IEnumerable<IConsumer<TEvent>>;

            foreach (var consumer in consumers)
            {
                //try to handle published event
                consumer.HandleEvent(@event);
            }
        }
    }
}