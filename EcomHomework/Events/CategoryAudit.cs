﻿using EcomHomework.Core.Events;
using EcomHomework.Models;
using Serilog;

namespace EcomHomework.Events
{
    public class CategoryAudit : IConsumer<EntityInsertedEvent<Category>>,
        IConsumer<EntityUpdatedEvent<Category>>,
        IConsumer<EntityDeletedEvent<Category>>
    {
        public void HandleEvent(EntityInsertedEvent<Category> eventMessage)
        {
            Log.Information("Category created");
        }

        public void HandleEvent(EntityUpdatedEvent<Category> eventMessage)
        {
            Log.Information("Category updated");
        }

        public void HandleEvent(EntityDeletedEvent<Category> eventMessage)
        {
            Log.Information("Category deleted");
        }
    }
}