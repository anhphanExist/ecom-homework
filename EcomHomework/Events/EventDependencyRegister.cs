﻿using System;
using EcomHomework.Core.Events;
using Microsoft.Extensions.DependencyInjection;

namespace EcomHomework.Events
{
    public static class EventDependencyRegister
    {
        public static void RegisterEvents(this IServiceCollection services)
        {
            services.AddSingleton<IEventPublisher, EventPublisher>();
            
            services.Scan(scan =>
                scan.FromAssemblies(AppDomain.CurrentDomain.GetAssemblies())
                    .AddClasses(classes =>
                        classes.AssignableTo(typeof(IConsumer<>)).Where(x => !x.IsGenericType))
                    .AsImplementedInterfaces()
                    .WithTransientLifetime());
        }
    }
}