﻿using Microsoft.Extensions.DependencyInjection;

namespace EcomHomework.Filters
{
    public static class FiltersDependencyRegister
    {
        public static void RegisterFilters(this IServiceCollection services)
        {
            services.AddScoped<TransactionalFilter>();
        }
    }
}