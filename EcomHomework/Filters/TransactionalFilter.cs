﻿using System;
using EcomHomework.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore.Storage;

namespace EcomHomework.Filters
{
    public class TransactionalFilter : IActionFilter
    {
        private readonly ApplicationDbContext _dbContext;
        private IDbContextTransaction _transaction;
        
        public TransactionalFilter(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void OnActionExecuting(ActionExecutingContext actionExecutingContext)
        {
            _transaction = _dbContext.Database.BeginTransaction();
        }

        public void OnActionExecuted(ActionExecutedContext actionExecutedContext)
        {
            try
            {
                _dbContext.SaveChanges();
                _transaction.Commit();
            }
            catch (Exception)
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
            }
        }
    }

    public class TransactionalAttribute : TypeFilterAttribute
    {
        public TransactionalAttribute() : base(typeof(TransactionalFilter))
        {
        }
    }
}