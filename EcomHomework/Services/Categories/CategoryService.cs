﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EcomHomework.Core.Events;
using EcomHomework.Core.Middlewares.ExceptionHandlers;
using EcomHomework.Core.Services;
using EcomHomework.Core.Services.Paging;
using EcomHomework.Core.Services.Searching;
using EcomHomework.Core.Services.Shaping;
using EcomHomework.Data;
using EcomHomework.Models;
using Microsoft.EntityFrameworkCore;

namespace EcomHomework.Services.Categories
{
    public interface ICategoryService : ICrudService<Guid, Category, CategorySearchParams>
    {
        
    }
    
    public class CategoryService : ICategoryService,
        ISearchable<Category, CategorySearchParams>,
        IOrderable<Category, CategorySearchParams>,
        IShapable<Category, CategorySearchParams>
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IEventPublisher _eventPublisher;

        public CategoryService(ApplicationDbContext dbContext, IEventPublisher eventPublisher)
        {
            _dbContext = dbContext;
            _eventPublisher = eventPublisher;
        }

        public async Task<IPagedList<Category>> Get(CategorySearchParams searchParams)
        {
            var query = _dbContext.Categories.AsNoTracking();
            query = Search(query, searchParams);
            query = Order(query, searchParams);
            query = query.Paginate(searchParams.CurrentPage, searchParams.PageSize, out var totalCount);
            query = Projection(query, searchParams);
            var items = await query.ToListAsync();
            return items.ToPagedList(searchParams.CurrentPage, searchParams.PageSize, totalCount);
        }

        public async Task<Category> GetById(Guid id)
        {
            var item = await _dbContext.Categories.AsNoTracking()
                .Select(c => new Category()
                {
                    Id = c.Id,
                    Name = c.Name,
                    Description = c.Description
                })
                .FirstOrDefaultAsync(c => c.Id == id);
            return item;
        }

        public async Task<Category> Create(Category model)
        {
            var exist = await _dbContext.Categories.AsNoTracking()
                .AnyAsync(q => q.Name.Equals(model.Name));
            if (exist)
            {
                throw new ValidationException("The category already exists");
            }

            var category = new Category()
            {
                Id = Guid.NewGuid(),
                Name = model.Name,
                Description = model.Description
            };
            
            await _dbContext.Categories.AddAsync(category);
            
            _eventPublisher.Publish(new EntityInsertedEvent<Category>(category));
            return category;
        }

        public async Task<Category> Update(Category model)
        {
            var category = await _dbContext.Categories
                .FirstOrDefaultAsync(q => q.Id == model.Id);
            if (category == default)
            {
                throw new ValidationException("The category doesn't exist");
            }

            category.Name = model.Name;
            category.Description = model.Description;

            return category;
        }

        public async Task Remove(Guid id)
        {
            var category = await _dbContext.Categories
                .FirstOrDefaultAsync(q => q.Id == id);

            if (category == default)
            {
                throw new ValidationException("The category doesn't exist");
            }

            _dbContext.Categories.Remove(category);
        }

        public IQueryable<Category> Search(IQueryable<Category> query, CategorySearchParams searchParams)
        {
            if (searchParams is null)
                return query.Where(q => false);
            
            if (!string.IsNullOrWhiteSpace(searchParams.SearchString))
                query = query.Where(q => 
                    q.Name.Contains(searchParams.SearchString));
            
            return query;
        }

        public IQueryable<Category> Order(IQueryable<Category> query, CategorySearchParams searchParams)
        {
            switch (searchParams.OrderBy)
            {
                case CategoryOrder.Name:
                    query = searchParams.IsDescending ? query.OrderByDescending(q => q.Name) : query.OrderBy(q => q.Name);
                    break;
                default:
                    query = searchParams.IsDescending ? query.OrderByDescending(q => q.Cx) : query.OrderBy(q => q.Cx);
                    break;
            }

            return query;
        }

        public IQueryable<Category> Projection(IQueryable<Category> query, CategorySearchParams searchParams)
        {
            return query.Select(q => new Category()
            {
                Id = searchParams.Projection.Contains(CategoryProjection.Id) ? q.Id : new Guid(),
                Name = searchParams.Projection.Contains(CategoryProjection.Name) ? q.Name : null,
                Description = searchParams.Projection.Contains(CategoryProjection.Description) ? q.Description : null,
                CreatedAt = searchParams.Projection.Contains(CategoryProjection.CreatedAt) ? q.CreatedAt : new DateTime(),
                UpdatedAt = searchParams.Projection.Contains(CategoryProjection.UpdatedAt) ? q.UpdatedAt : new DateTime(),
                DeletedAt = searchParams.Projection.Contains(CategoryProjection.DeletedAt) ? q.DeletedAt : new DateTime()
            });
        }
    }
}