﻿using System;
using EcomHomework.Core.Services.Searching;
using EcomHomework.Core.Services.Shaping;

namespace EcomHomework.Services.Categories
{
    public class CategorySearchParams : SearchParamsBase
    {
        public CategoryOrder? OrderBy { get; set; }
        public CategoryProjection Projection { get; set; }
    }

    public enum CategoryOrder
    {
        Cx,
        Name
    }
    
    [Flags]
    public enum CategoryProjection : long
    {
        All = DataShaper.All,
        Id = DataShaper._1,
        CreatedAt = DataShaper._2,
        UpdatedAt = DataShaper._3,
        DeletedAt = DataShaper._4,
        Name = DataShaper._5,
        Description = DataShaper._6,
        
    }
}