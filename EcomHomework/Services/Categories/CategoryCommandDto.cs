﻿using System;
using EcomHomework.Core.Dtos;

namespace EcomHomework.Services.Categories
{
    public class CategoryCommandDto : CommandDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}