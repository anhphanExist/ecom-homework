﻿using EcomHomework.Services.Categories;
using Microsoft.Extensions.DependencyInjection;

namespace EcomHomework.Services
{
    public static class ServicesLayerDependencyRegister
    {
        public static void RegisterServicesLayer(this IServiceCollection services)
        {
            services.AddScoped<ICategoryService, CategoryService>();
        }
    }
}