﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EcomHomework.Core.Controllers;
using EcomHomework.Core.Services.Paging;
using EcomHomework.Core.Validators;
using EcomHomework.Filters;
using EcomHomework.Models;
using EcomHomework.Services.Categories;
using Microsoft.AspNetCore.Mvc;

namespace EcomHomework.Controllers
{
    public class CategoriesController : ApiController
    {
        private readonly IMapper _mapper;
        private readonly ICategoryService _categoryService;

        public CategoriesController(IMapper mapper, ICategoryService categoryService)
        {
            _mapper = mapper;
            _categoryService = categoryService;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] int currentPage, [FromQuery] int pageSize, 
            [FromQuery] string searchString, [FromQuery] CategoryOrder? sortBy, [FromQuery] bool isDescending, 
            [FromQuery] IEnumerable<CategoryProjection> projection)
        {
            var searchParams = new CategorySearchParams()
            {
                SearchString = searchString,
                OrderBy = sortBy,
                IsDescending = isDescending,
                CurrentPage = currentPage,
                PageSize = pageSize,
                Projection = projection.Aggregate<CategoryProjection, CategoryProjection>(0, (current, p) => current | p)
            };
            var categories = await _categoryService.Get(searchParams);
            var result = _mapper.Map<IPagedList<CategoryDto>>(categories);
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById([FromRoute] Guid id)
        {
            var category = await _categoryService.GetById(id);
            var result = _mapper.Map<CategoryDto>(category);
            return Ok(result);
        }

        [HttpPost]
        [ValidateModel]
        [Transactional]
        public async Task<IActionResult> Create([FromBody] CategoryCommandDto dto)
        {
            var command = _mapper.Map<Category>(dto);
            var category = await _categoryService.Create(command);
            var result = _mapper.Map<CategoryDto>(category);
            return CreatedAtAction("GetById", new {id = result.Id}, result);
        }

        [HttpPut]
        [ValidateModel]
        [Transactional]
        public async Task<IActionResult> Update([FromBody] CategoryCommandDto dto)
        {
            var command = _mapper.Map<Category>(dto);
            var category = await _categoryService.Update(command);
            var result = _mapper.Map<CategoryDto>(category);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        [Transactional]
        public async Task<IActionResult> Remove([FromRoute] Guid id)
        {
            await _categoryService.Remove(id);
            return Ok();
        }
    }
}