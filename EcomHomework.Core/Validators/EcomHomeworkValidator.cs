﻿using EcomHomework.Core.Dtos;
using FluentValidation;

namespace EcomHomework.Core.Validators
{
    public abstract class EcomHomeworkValidator<T> : AbstractValidator<T>
        where T : CommandDto
    {
        
    }
}