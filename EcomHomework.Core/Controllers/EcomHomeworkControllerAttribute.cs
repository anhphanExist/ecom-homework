﻿using System;
using Microsoft.AspNetCore.Mvc.Routing;

namespace EcomHomework.Core.Controllers
{
    public class EcomHomeworkControllerAttribute : Attribute, IRouteTemplateProvider
    {
        public string Template => "api/[controller]";
        public int? Order => 2;
        public string Name { get; set; }
    }
}