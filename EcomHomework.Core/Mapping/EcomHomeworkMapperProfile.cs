﻿using System;
using AutoMapper;
using EcomHomework.Core.Dtos;
using EcomHomework.Core.Models;
using EcomHomework.Core.Services.Paging;

namespace EcomHomework.Core.Mapping
{
    public abstract class EcomHomeworkMapperProfile<T, TResponseDto, TCommandDto> : Profile
        where T : IBaseEntity
        where TResponseDto : ResponseDto
        where TCommandDto : CommandDto
    {
        protected EcomHomeworkMapperProfile()
        {
            CreateMap<IBaseEntity, ResponseDto>()
                .ForMember(dto => dto.CreatedAt, options => options
                    .MapFrom((entity, dto) =>
                    {
                        dto.CreatedAt = entity.CreatedAt != DateTime.MinValue ? entity.CreatedAt : (DateTime?) null;
                        return dto.CreatedAt;
                    }))
                .ForMember(dto => dto.UpdatedAt, options => options
                    .MapFrom((entity, dto) =>
                    {
                        dto.UpdatedAt = entity.UpdatedAt != DateTime.MinValue ? entity.UpdatedAt : (DateTime?) null;
                        return dto.UpdatedAt;
                    }))
                .ForMember(dto => dto.DeletedAt, options => options
                    .MapFrom((entity, dto) =>
                    {
                        dto.DeletedAt = entity.DeletedAt != DateTime.MinValue ? entity.DeletedAt : (DateTime?) null;
                        return dto.DeletedAt;
                    }));
            
            CreateMap<IPagedList<IBaseEntity>, IPagedList<ResponseDto>>();

            CreateMap<CommandDto, IBaseEntity>();

            CreateMap<T, TResponseDto>()
                .IncludeBase<IBaseEntity, ResponseDto>();

            CreateMap<TCommandDto, T>()
                .IncludeBase<CommandDto, IBaseEntity>();
        }
    }
}