﻿using System.Linq;
using EcomHomework.Core.Models;
using EcomHomework.Core.Services.Searching;

namespace EcomHomework.Core.Services.Shaping
{
    public interface IShapable<T, TSearchParams>
        where T : BaseEntity
        where TSearchParams : SearchParamsBase
    {
        IQueryable<T> Projection(IQueryable<T> query, TSearchParams searchParams);
    }
}