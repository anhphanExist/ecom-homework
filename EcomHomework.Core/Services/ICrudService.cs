﻿using System.Threading.Tasks;
using EcomHomework.Core.Services.Paging;
using EcomHomework.Core.Services.Searching;

namespace EcomHomework.Core.Services
{
    public interface ICrudService<TId, T, TSearchParams> 
        where TSearchParams : SearchParamsBase
    {
        Task<IPagedList<T>> Get(TSearchParams searchParams);
        Task<T> GetById(TId id);
        Task<T> Create(T model);
        Task<T> Update(T model);
        Task Remove(TId id);
    }
}