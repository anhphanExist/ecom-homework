﻿using System.Linq;

namespace EcomHomework.Core.Services.Searching
{
    public interface IOrderable<T, TSearchParams> where TSearchParams : SearchParamsBase
    {
        IQueryable<T> Order(IQueryable<T> query, TSearchParams searchParams);
    }
}