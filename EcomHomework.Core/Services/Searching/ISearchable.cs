﻿using System.Linq;

namespace EcomHomework.Core.Services.Searching
{
    public interface ISearchable<T, in TSearchParams> where TSearchParams : SearchParamsBase
    {
        public IQueryable<T> Search(IQueryable<T> query, TSearchParams searchParams);
    }
}