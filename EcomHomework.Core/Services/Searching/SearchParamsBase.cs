﻿namespace EcomHomework.Core.Services.Searching
{
    public abstract class SearchParamsBase
    {
        private const int MaxPageSize = 100;
        private int _currentPage;
        private int _pageSize;

        public int CurrentPage
        {
            get => _currentPage;
            set => _currentPage = value == default ? 1 : value;
        }

        public int PageSize
        {
            get => _pageSize;
            set
            {
                if (value == -1)
                {
                    _pageSize = int.MaxValue;
                    return;
                }
                _pageSize = (value == default || value > MaxPageSize) ? MaxPageSize : value;
            } 
        }
        public string SearchString { get; set; }
        public bool IsDescending { get; set; }

        public SearchParamsBase()
        {
            CurrentPage = 1;
            PageSize = MaxPageSize;
        }
    }
}