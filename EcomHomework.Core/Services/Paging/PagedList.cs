﻿using System;
using System.Collections.Generic;

namespace EcomHomework.Core.Services.Paging
{
    public interface IPagedList<T>
    {
        int CurrentPage { get; set; }
        int PageSize { get; set; }
        int TotalItems { get; set; }
        int TotalPages { get; set; }
        bool HasPreviousPage { get; }
        bool HasNextPage { get; }
        IList<T> Items { get; set; }
    }
    
    [Serializable]
    public class PagedList<T> : IPagedList<T>
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages { get; set; }
        public bool HasPreviousPage => CurrentPage > 0;
        public bool HasNextPage => CurrentPage + 1 < TotalPages;
        public IList<T> Items { get; set; }
    }
}