﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EcomHomework.Core.Services.Paging
{
    public static class DataPagerExtension
    {
        public static PagedList<T> ToPagedList<T>(this IList<T> items, int page, int pageSize,
            int totalCount)
        {
            var res = new PagedList<T>()
            {
                TotalItems = totalCount,
                CurrentPage = page,
                PageSize = pageSize,
                TotalPages = (int) Math.Ceiling(totalCount / (double) pageSize),
                Items = items
            };
            return res;
        }

        public static IQueryable<TSource> Paginate<TSource>(this IQueryable<TSource> query, int page, int pageSize,
            out int totalCount)
        {
            var startRow = (page - 1) * pageSize;
            totalCount = query.Count();
            return query
                .Skip(startRow)
                .Take(pageSize);
        }
    }
}