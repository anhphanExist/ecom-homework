﻿using System;

namespace EcomHomework.Core.Middlewares.ExceptionHandlers
{
    public class NotFoundException : Exception
    {
        public const int StatusCode = 404;
        private const string DefaultMessage = "Resource not found";

        public NotFoundException() : base(DefaultMessage)
        {
            
        }
        
        public NotFoundException(string message) : base(message)
        {

        }

        public NotFoundException(Exception ex) : base(ex.Message)
        {

        }
    }
}