﻿using System;
using System.Diagnostics;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace EcomHomework.Core.Middlewares.ExceptionHandlers
{
    public static class ExceptionHandlingMiddlewareExtensions
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app, IWebHostEnvironment env,
            ILogger logger)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    await HandleExceptionAsync(context, logger, true);
                });
            });
        }

        private static async Task HandleExceptionAsync(HttpContext context, ILogger logger, bool includeDetails)
        {
            var exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerFeature>();
            if (exceptionHandlerPathFeature?.Error is null)
                return;

            // ProblemDetails has it's own content type
            context.Response.ContentType = "application/problem+json";

            var error = exceptionHandlerPathFeature.Error;
            var title = includeDetails ? error.Message : "An error occured";
            var details = includeDetails ? error.ToString() : null;
            var status = GetCustomStatusCode(error);

            if (status == 500)
            {
                logger.LogError($"Warning: error code {status}. {title}{details}");
            }
            else
            {
                logger.LogWarning($"Warning: error code {status}. {title}{details}");
            }

            var problem = new ProblemDetails
            {
                Status = status,
                Title = title,
                Detail = details
            };

            // This is often very handy information for tracing the specific request
            var traceId = Activity.Current?.Id ?? context?.TraceIdentifier;
            problem.Extensions["traceId"] = traceId;

            //Serialize the problem details object to the Response as JSON (using System.Text.Json)
            context.Response.StatusCode = status;
            var stream = context.Response.Body;
            await JsonSerializer.SerializeAsync(stream, problem);
        }

        private static int GetCustomStatusCode(Exception error)
        {
            if (error is ValidationException)
                return ValidationException.StatusCode;
            if (error is UnauthorizedException)
                return UnauthorizedException.StatusCode;
            if (error is NotFoundException)
                return NotFoundException.StatusCode;

            return (int) HttpStatusCode.InternalServerError;
        }
    }
}