﻿using System;
using System.Text.Json;

namespace EcomHomework.Core.Middlewares.ExceptionHandlers
{
    public class ValidationException : Exception
    {
        public const int StatusCode = 400;

        public static string ModifyMessage(object result)
        {
            return JsonSerializer.Serialize(result);
        }

        public ValidationException(string message) : base(message)
        {

        }

        public ValidationException(Exception ex) : base(ex.Message)
        {

        }
    }
}