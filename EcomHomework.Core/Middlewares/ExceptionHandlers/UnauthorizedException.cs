﻿using System;

namespace EcomHomework.Core.Middlewares.ExceptionHandlers
{
    public class UnauthorizedException : Exception
    {
        public const int StatusCode = 401;
        private const string DefaultMessage = "You don't have permission to access this resource";
        
        public UnauthorizedException() : base(DefaultMessage)
        {
            
        }
        
        public UnauthorizedException(string message) : base(message)
        {

        }

        public UnauthorizedException(Exception ex) : base(ex.Message)
        {

        }
    }
}