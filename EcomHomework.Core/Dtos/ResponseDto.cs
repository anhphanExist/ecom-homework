﻿using System;

namespace EcomHomework.Core.Dtos
{
    public abstract class ResponseDto
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}